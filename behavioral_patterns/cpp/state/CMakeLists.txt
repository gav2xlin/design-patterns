cmake_minimum_required(VERSION 3.5)

project(state LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

add_executable(state main.cpp)

install(TARGETS state
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})
